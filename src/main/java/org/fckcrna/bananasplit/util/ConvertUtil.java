package org.fckcrna.bananasplit.util;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;

public class ConvertUtil {

    public static String blobToBase64String(Blob blob) throws Exception {
        byte[] bytes = blobToByteArray(blob);
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static byte[] blobToByteArray(Blob blob) throws Exception {
        if (blob != null) {
            int length = (int) blob.length();
            byte[] bytes = blob.getBytes(1, length);
            return bytes;
        }
        return new byte[0];
    }

    public static byte[] base64StringToByteArray(String s) {
        if (s != null) {
            return Base64.getDecoder().decode(s);
        }
        return new byte[0];
    }

    public static Blob base64StringToBlob(String s) throws SQLException {
        byte[] bytes = base64StringToByteArray(s);
        if (bytes.length > 0) {
            return new SerialBlob(bytes);
        }
        return null;
    }
}