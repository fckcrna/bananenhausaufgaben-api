package org.fckcrna.bananasplit.constants;

/**
 * @author Felix Bender
 * @version 1.0
 */
public enum TaskState {
    TODO(0),
    REVIEW(1),
    FINISHED(2),
    FAILED(3),
    SEEN(4);

    private Integer value;

    TaskState(int state) {
        this.value = state;
    }

    public Integer value() {
        return value;
    }
}
