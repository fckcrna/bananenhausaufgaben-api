package org.fckcrna.bananasplit.api.v1.student;

import org.fckcrna.bananasplit.api.v1.model.classstudent.ClassStudent;
import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;
import org.fckcrna.bananasplit.api.v1.model.student.Student;
import org.fckcrna.bananasplit.api.v1.model.student.StudentBuilder;
import org.fckcrna.bananasplit.api.v1.model.student.StudentForTeacherDto;
import org.fckcrna.bananasplit.api.v1.model.task.Task;
import org.fckcrna.bananasplit.api.v1.model.taskstudent.TaskStudent;
import org.fckcrna.bananasplit.constants.TaskState;
import org.fckcrna.bananasplit.util.HibernateUtil;
import org.fckcrna.bananasplit.util.PasswordUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Felix Bender
 * @version 1.0
 */
@Service
public class StudentService {

    public StudentForTeacherDto createStudentInClass(JSONObject json) {
        return createStudentInClass((String) json.get("nickname"), (Long) json.get("classId"));
    }

    private StudentForTeacherDto createStudentInClass(String name, Long classId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Learngroup> cq = cb.createQuery(Learngroup.class);
        Root<Learngroup> root = cq.from(Learngroup.class);

        Predicate p = cb.equal(root.get("id"), classId);
        cq.select(root).where(p);

        Query<Learngroup> query = session.createQuery(cq);
        Learngroup learngroup = query.getSingleResult();

        String password = PasswordUtil.generatePasswordString();
        String hashedPassword = PasswordUtil.encryptPassword(password);

        Student student = new StudentBuilder()
                .setGroup(learngroup)
                .setNickName(name + learngroup.getId())
                .setPassword(hashedPassword)
                .build();


        session.save(student);

        // save class student
        ClassStudent cs = new ClassStudent();
        cs.setStudent(student);
        cs.setGroup(learngroup);

        session.save(cs);

        // Add all tasks to user where due date is not reached
        CriteriaQuery<Task> cqt = cb.createQuery(Task.class);
        Root<Task> taskRoot = cqt.from(Task.class);

        Predicate c = cb.equal(taskRoot.get("learngroup"), learngroup.getId());
        Predicate dd = cb.greaterThan(taskRoot.get("dueDate"), System.currentTimeMillis());
        cqt.select(taskRoot).where(cb.and(c, dd));

        Query<Task> taskQuery = session.createQuery(cqt);
        List<Task> tasks = taskQuery.getResultList();

        for (Task t : tasks) {
            TaskStudent ts = new TaskStudent(t, student, "", null, TaskState.TODO.ordinal(), null);
            session.save(ts);
        }

        session.close();
        return student.toForTeacherDto(password);
    }

    public List<StudentForTeacherDto> createStudentsInClass(JSONObject json) {
        List<String> names = (List<String>) json.get("names");
        Long classId = (Long) json.get("classId");
        List<StudentForTeacherDto> dtos = new ArrayList<>();

        for (String name : names) {
            dtos.add(createStudentInClass(name, classId));
        }

        return dtos;
    }

    public void deleteStudent(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        int rows = 0;

        String hql = "delete from TaskStudent where student.id = :id";
        rows += session.createQuery(hql).setParameter("id", id).executeUpdate();

        hql = "delete from ClassStudent where student.id = :id";
        rows += session.createQuery(hql).setParameter("id", id).executeUpdate();

        hql = "delete from Student where id = :id";
        rows += session.createQuery(hql).setParameter("id", id).executeUpdate();

        if (rows == 0) {
            tx.rollback();
            session.close();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There was no Student with that ID");
        }
        tx.commit();
        session.close();
    }
}
