package org.fckcrna.bananasplit.api.v1.model.learnplace;

import org.fckcrna.bananasplit.api.v1.model.task.TaskDto;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class TaskStudentForLearnplaceDto {

    private Long id;
    private TaskDto task;
    private Long student;
    private String note;
    private String picture;
    private Integer state;
    private Long submissionDate;

    public TaskStudentForLearnplaceDto() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaskDto getTask() {
        return task;
    }

    public void setTask(TaskDto task) {
        this.task = task;
    }

    public Long getStudent() {
        return student;
    }

    public void setStudent(Long student) {
        this.student = student;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Long submissionDate) {
        this.submissionDate = submissionDate;
    }
}
