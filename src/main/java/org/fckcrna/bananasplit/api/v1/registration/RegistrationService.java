package org.fckcrna.bananasplit.api.v1.registration;

import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;
import org.fckcrna.bananasplit.api.v1.model.teacher.TeacherDto;
import org.fckcrna.bananasplit.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service()
public class RegistrationService {

    public TeacherDto saveTeacher(Teacher teacher) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        try {
            session.save(teacher);
        } catch (Exception e) {
            tx.rollback();
            session.close();
            throw new ResponseStatusException(HttpStatus.CONFLICT, "User can't be created", e);
        }
        tx.commit();
        session.close();
        return teacher.toDto();
    }
}