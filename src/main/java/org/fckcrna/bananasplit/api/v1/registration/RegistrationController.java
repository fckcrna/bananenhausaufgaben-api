package org.fckcrna.bananasplit.api.v1.registration;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.fckcrna.bananasplit.api.v1.model.teacher.TeacherBuilder;
import org.fckcrna.bananasplit.api.v1.model.teacher.TeacherDto;
import org.fckcrna.bananasplit.util.PasswordUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1/registration")
@Api(value = "Registration", description = "Registers a Teacher")
public class RegistrationController {

    @Autowired
    RegistrationService service;

    @ApiOperation(value = "Register a teacher with the given Attributes", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created Teacher"),
            @ApiResponse(code = 500, message = "Failed to Create Teacher")
    })
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/registerTeacher")
    public TeacherDto registerTeacher(
            @RequestBody final String body) {

        JSONParser parser = new JSONParser();

        try {
            JSONObject json = (JSONObject) parser.parse(body);
            String clean = (String) json.get("password");
            String hashed = PasswordUtil.encryptPassword(clean);
            TeacherBuilder builder = new TeacherBuilder()
                    .setMail((String) json.get("mail"))
                    .setFirstName((String) json.get("firstname"))
                    .setLastName((String) json.get("lastname"))
                    .setPassword(hashed);

            return service.saveTeacher(builder.build());
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Body was malformed", e);
        }

    }
}