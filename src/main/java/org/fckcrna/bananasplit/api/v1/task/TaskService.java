package org.fckcrna.bananasplit.api.v1.task;

import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;
import org.fckcrna.bananasplit.api.v1.model.learnplace.TaskStudentForLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.student.Student;
import org.fckcrna.bananasplit.api.v1.model.task.Task;
import org.fckcrna.bananasplit.api.v1.model.task.TaskBuilder;
import org.fckcrna.bananasplit.api.v1.model.task.TaskDetailDto;
import org.fckcrna.bananasplit.api.v1.model.task.TaskDto;
import org.fckcrna.bananasplit.api.v1.model.taskstudent.TaskStudent;
import org.fckcrna.bananasplit.api.v1.model.taskstudent.TaskStudentDto;
import org.fckcrna.bananasplit.api.v1.model.taskstudent.TaskStudentForDetailDto;
import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;
import org.fckcrna.bananasplit.constants.TaskState;
import org.fckcrna.bananasplit.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Felix Bender
 * @version 1.0
 */
@Service
public class TaskService {

    public TaskDto createTask(JSONObject json, TaskBuilder builder) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        // get Teacher for Task
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Teacher> cqt = cb.createQuery(Teacher.class);
        Root<Teacher> root = cqt.from(Teacher.class);

        Predicate teacherPredicate = cb.equal(root.get("id"), (Long) json.get("teacherId"));
        cqt.select(root).where(teacherPredicate);

        Query<Teacher> query = session.createQuery(cqt);
        Teacher teacher = query.uniqueResult();
        if (teacher != null)
            builder = builder.setOwner(teacher);
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Teacher not found");

        // get Class for Task
        CriteriaQuery<Learngroup> cql = cb.createQuery(Learngroup.class);
        Root<Learngroup> rootL = cql.from(Learngroup.class);

        Predicate learngroupPredicate = cb.equal(rootL.get("id"), (Long) json.get("classId"));
        cql.select(rootL).where(learngroupPredicate);

        Query<Learngroup> learngroupQuery = session.createQuery(cql);
        Learngroup learngroup = learngroupQuery.uniqueResult();
        if (learngroup != null)
            builder = builder.setLearngroup(learngroup);
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Class not found");

        // save Task
        Task task = builder.build();
        session.save(task);

        // get all Students of class
        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
        Root<Student> s = cq.from(Student.class);
        Predicate p = cb.equal(s.get("group"), learngroup);
        cq.select(s).where(p);
        Query<Student> studentQuery = session.createQuery(cq);
        List<Student> students = studentQuery.getResultList();

        for (Student student : students) {
            TaskStudent ts = new TaskStudent();
            ts.setState(TaskState.TODO.ordinal());
            ts.setTask(task);
            ts.setStudent(student);
            session.save(ts);
        }

        session.close();

        return task.toDto();
    }

    public TaskStudentDto submitTask(JSONObject json) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        String hql = "update TaskStudent set state = :state, note = :note, submissionDate = :date where id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("state", TaskState.REVIEW.value());
        query.setParameter("note", json.get("note"));
        query.setParameter("date", System.currentTimeMillis());
        query.setParameter("id", json.get("id"));
        int rows = query.executeUpdate();

        if (rows != 1) {
            tx.rollback();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Not one TaskStudent found");
        }
        tx.commit();

        hql = "from TaskStudent where id = :id";
        query = session.createQuery(hql).setParameter("id", json.get("id"));
        TaskStudent ts = (TaskStudent) query.uniqueResult();

        session.close();

        return ts.toDto();
    }

    public TaskDetailDto getStudentsForTask(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<TaskStudentForDetailDto> dtos = new ArrayList<>();

        String hql = "from TaskStudent where task.id = :id";
        List<TaskStudent> students = session.createQuery(hql).setParameter("id", id).getResultList();

        for (TaskStudent ts : students) {
            dtos.add(ts.toDetailDto());
        }
        TaskDetailDto dto = new TaskDetailDto();
        dto.setStudents(dtos);

        session.close();
        return dto;
    }

    public TaskStudentForLearnplaceDto setTaskSeen(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        String hql = "update TaskStudent set state = :state where id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("state", TaskState.SEEN.value());
        query.setParameter("id", id);

        int result = query.executeUpdate();

        if (result != 1) {
            tx.rollback();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "More than one TaskStudent found");
        }
        tx.commit();

        hql = "from TaskStudent where id = :id";
        query = session.createQuery(hql).setParameter("id", id);
        TaskStudent ts = (TaskStudent) query.uniqueResult();

        session.close();
        return ts.toLearnplaceDto();
    }

    public TaskStudentForDetailDto approveTaskByTeacher(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        String hql = "update TaskStudent set state = :state where id = :id and state = :review";
        Query query = session.createQuery(hql).setParameter("state", TaskState.FINISHED.value()).setParameter("id", id).setParameter("review", TaskState.REVIEW.value());
        int rows = query.executeUpdate();

        if (rows > 1) {
            tx.rollback();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "More than one TaskStudent found");
        } else if (rows == 0) {
            tx.rollback();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The Task can't be approved");
        }

        hql = "from TaskStudent where id = :id";
        query = session.createQuery(hql).setParameter("id", id);
        TaskStudent ts = (TaskStudent) query.uniqueResult();

        session.close();
        return ts.toDetailDto();
    }
}
