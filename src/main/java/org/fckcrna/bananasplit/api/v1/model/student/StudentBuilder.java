package org.fckcrna.bananasplit.api.v1.model.student;

import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class StudentBuilder {

    private Long id;
    private String nickName;
    private Learngroup group;
    private String password;

    public StudentBuilder() {

    }

    public StudentBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public StudentBuilder setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public StudentBuilder setGroup(Learngroup group) {
        this.group = group;
        return this;
    }

    public StudentBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public Student build() {
        return new Student(id, nickName, password, group);
    }
}
