package org.fckcrna.bananasplit.api.v1.model.classstudent;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;
import org.fckcrna.bananasplit.api.v1.model.student.Student;

@Entity
@Table(name = "class_student")
public class ClassStudent implements Serializable {

    private static final long serialVersionUID = -4990680528839662591L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "class")
    private Learngroup group;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student")
    private Student student;

    public ClassStudent() {

    }

    public ClassStudent(Integer id, Learngroup group, Student student) {
        this.id = id;
        this.group = group;
        this.student = student;
    }

    public Integer getId() {
        return id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Learngroup getGroup() {
        return group;
    }

    public void setGroup(Learngroup group) {
        this.group = group;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}