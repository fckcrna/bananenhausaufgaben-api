package org.fckcrna.bananasplit.api.v1.model.task;

import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;
import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;

import java.sql.Blob;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class TaskBuilder {
    private Long id;
    private Teacher owner;
    private Learngroup learngroup;
    private String title;
    private Blob picture;
    private String text;
    private Long dueDate;
    private String subject;
    private Long creationDate;

    public TaskBuilder() {

    }

    public TaskBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public TaskBuilder setOwner(Teacher owner) {
        this.owner = owner;
        return this;
    }

    public TaskBuilder setLearngroup(Learngroup learngroup) {
        this.learngroup = learngroup;
        return this;
    }

    public TaskBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public TaskBuilder setPicture(Blob picture) {
        this.picture = picture;
        return this;
    }

    public TaskBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public TaskBuilder setDueDate(Long dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public TaskBuilder setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public TaskBuilder setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public Task build() {
        return new Task(id, owner, learngroup, title, picture, text, dueDate, subject, creationDate);
    }
}
