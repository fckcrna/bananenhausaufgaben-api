package org.fckcrna.bananasplit.api.v1.model.teacher;

public class TeacherBuilder {
    private String mail;
    private String firstName;
    private String lastName;
    private String password;

    public TeacherBuilder() {

    }

    public TeacherBuilder setMail(String mail) {
        this.mail = mail;
        return this;
    }

    public TeacherBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TeacherBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TeacherBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public Teacher build() {
        return new Teacher(null, mail, firstName, lastName, password);
    }
}