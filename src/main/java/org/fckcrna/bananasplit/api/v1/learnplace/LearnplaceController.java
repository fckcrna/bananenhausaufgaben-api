package org.fckcrna.bananasplit.api.v1.learnplace;

import org.fckcrna.bananasplit.api.v1.model.learnplace.StudentLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.learnplace.TeacherLearnplaceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Felix Bender
 * @version 1.0
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/v1/learnplace")
public class LearnplaceController {

    @Autowired
    LearnplaceService service;

    @GetMapping("/teacher")
    public TeacherLearnplaceDto getLearnplaceForTeacher(@RequestParam(value = "id") final String idString) {
        Integer id = null;
        try {
            id = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The ID was not an Integer", e);
        }

        TeacherLearnplaceDto dto = service.getTeacherLearnplaceById(id);

        return dto;
    }

    @GetMapping("/student")
    public StudentLearnplaceDto getLearnplaceForUser(@RequestParam(value = "id") final String idString) {
        Integer id = null;
        try {
            id = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The ID was not an Integer", e);
        }

        StudentLearnplaceDto dto = service.getStudentLearnplaceById(id);

        return dto;
    }
}
