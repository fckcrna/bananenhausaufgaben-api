package org.fckcrna.bananasplit.api.v1.model.task;

public class TaskDto {

    private Long id;
    private Integer owner;
    private Integer learngroup;
    private String learngroupName;
    private String title;
    private String picture;
    private String text;
    private Long dueDate;
    private String subject;
    private Long creationDate;

    public TaskDto() {

    }

    public Long getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getLearngroup() {
        return learngroup;
    }

    public void setLearngroup(Integer learngroup) {
        this.learngroup = learngroup;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public String getLearngroupName() {
        return learngroupName;
    }

    public void setLearngroupName(String learngroupName) {
        this.learngroupName = learngroupName;
    }
}