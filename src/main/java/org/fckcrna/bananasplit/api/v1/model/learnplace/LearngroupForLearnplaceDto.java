package org.fckcrna.bananasplit.api.v1.model.learnplace;

import org.fckcrna.bananasplit.api.v1.model.student.StudentForTeacherDto;
import org.fckcrna.bananasplit.api.v1.model.task.TaskDto;

import java.util.List;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class LearngroupForLearnplaceDto {

    private Integer id;
    private String name;
    private Integer teacherId;
    private String teacherName;
    private List<TaskDto> tasks;
    private List<StudentForTeacherDto> students;

    public LearngroupForLearnplaceDto() {

    }

    public List<TaskDto> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskDto> tasks) {
        this.tasks = tasks;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<StudentForTeacherDto> getStudents() {
        return students;
    }

    public void setStudents(List<StudentForTeacherDto> students) {
        this.students = students;
    }
}
