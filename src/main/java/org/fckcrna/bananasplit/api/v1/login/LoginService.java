package org.fckcrna.bananasplit.api.v1.login;

import org.fckcrna.bananasplit.api.v1.model.student.Student;
import org.fckcrna.bananasplit.api.v1.model.student.StudentDto;
import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;
import org.fckcrna.bananasplit.api.v1.model.teacher.TeacherDto;
import org.fckcrna.bananasplit.util.HibernateUtil;
import org.fckcrna.bananasplit.util.PasswordUtil;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.security.auth.login.LoginException;

@Service
public class LoginService {

    public TeacherDto loginTeacher(String mail, String password) throws ObjectNotFoundException, LoginException {
        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Teacher> cq = cb.createQuery(Teacher.class);
        Root<Teacher> root = cq.from(Teacher.class);

        Predicate m = cb.equal(root.get("mail"), mail);
        cq.select(root).where(m);

        Query<Teacher> query = session.createQuery(cq);
        Teacher result = query.uniqueResult();

        if (result != null) {
            if (PasswordUtil.checkPassword(password, result.getPassword())) {
                session.close();
                return result.toDto();
            } else {
                session.close();
                throw new LoginException("Password not correct");
            }
        } else {
            session.close();
            throw new ObjectNotFoundException(Teacher.class, mail);
        }
    }

	public StudentDto loginStudent(String nickName, String password) throws ObjectNotFoundException, LoginException {
        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
        Root<Student> root = cq.from(Student.class);

        Predicate m = cb.equal(root.get("nickName"), nickName);
        cq.select(root).where(m);

        Query<Student> query = session.createQuery(cq);
        Student result = query.uniqueResult();

        if (result != null) {
            if (PasswordUtil.checkPassword(password, result.getPassword())) {
                session.close();
                return result.toDto();
            } else {
                session.close();
                throw new LoginException("Password not correct");
            }
        } else {
            session.close();
            throw new ObjectNotFoundException(Student.class, nickName);
        }
	}
}