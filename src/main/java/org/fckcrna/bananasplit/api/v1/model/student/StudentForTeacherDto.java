package org.fckcrna.bananasplit.api.v1.model.student;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class StudentForTeacherDto {

    private Long id;
    private String nickName;
    private Integer group;
    private String password;

    public StudentForTeacherDto() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
