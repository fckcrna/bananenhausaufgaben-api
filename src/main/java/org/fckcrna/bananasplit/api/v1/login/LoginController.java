package org.fckcrna.bananasplit.api.v1.login;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.fckcrna.bananasplit.api.v1.model.student.StudentDto;
import org.fckcrna.bananasplit.api.v1.model.teacher.TeacherDto;
import org.hibernate.ObjectNotFoundException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.security.auth.login.LoginException;

@CrossOrigin(origins = "")
@RestController
@RequestMapping(value = "/v1/login")
@Api(value = "Login", description = "Handles login for Teacher and Student")
public class LoginController {

    @Autowired
    private LoginService service;

    @ApiOperation(value = "Login Teacher with credentials")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful login"),
            @ApiResponse(code = 401, message = "Password or mail is wrong")
    })
    @PostMapping(value = "/teacher", produces = "application/json")
    public TeacherDto loginTeacher(
            @RequestBody final String body) {

        JSONParser parser = new JSONParser();

        try {
            JSONObject json = (JSONObject) parser.parse(body);
            TeacherDto dto = service.loginTeacher((String) json.get("mail"), (String) json.get("password"));
            return dto;
        } catch (ObjectNotFoundException | LoginException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Password was wrong or mail was wrong", e);
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Body was malformed", e);
        }
    }

    @ApiOperation(value = "Login Student with credentials")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Login successful"),
                    @ApiResponse(code = 401, message = "Password or nickname was wrong")
            }
    )
    @PostMapping(value = "/student", produces = "application/json")
    public StudentDto loginStudent(
            @RequestBody final String body) {

        JSONParser parser = new JSONParser();

        try {
            JSONObject json = (JSONObject) parser.parse(body);
            StudentDto dto = service.loginStudent((String) json.get("nickname"), (String) json.get("password"));
            return dto;
        } catch (ObjectNotFoundException | LoginException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Password or nickname was wrong", e);
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Body was malformed", e);
        }
    }
}