package org.fckcrna.bananasplit.api.v1.model.task;

import org.fckcrna.bananasplit.api.v1.model.taskstudent.TaskStudentForDetailDto;

import java.util.List;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class TaskDetailDto {
    private List<TaskStudentForDetailDto> students;

    public TaskDetailDto() {

    }

    public List<TaskStudentForDetailDto> getStudents() {
        return students;
    }

    public void setStudents(List<TaskStudentForDetailDto> students) {
        this.students = students;
    }
}
