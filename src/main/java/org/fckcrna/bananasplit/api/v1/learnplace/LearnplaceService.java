package org.fckcrna.bananasplit.api.v1.learnplace;

import org.fckcrna.bananasplit.api.v1.model.classstudent.ClassStudent;
import org.fckcrna.bananasplit.api.v1.model.classteacher.ClassTeacher;
import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;
import org.fckcrna.bananasplit.api.v1.model.learnplace.LearngroupForLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.learnplace.StudentLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.learnplace.TaskStudentForLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.learnplace.TeacherLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.student.StudentForTeacherDto;
import org.fckcrna.bananasplit.api.v1.model.task.Task;
import org.fckcrna.bananasplit.api.v1.model.task.TaskDto;
import org.fckcrna.bananasplit.api.v1.model.taskstudent.TaskStudent;
import org.fckcrna.bananasplit.constants.TaskState;
import org.fckcrna.bananasplit.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Felix Bender
 * @version 1.0
 */
@Service
public class LearnplaceService {

    public TeacherLearnplaceDto getTeacherLearnplaceById(Integer id) {
        TeacherLearnplaceDto dto = new TeacherLearnplaceDto();
        List<LearngroupForLearnplaceDto> l = new ArrayList<>();

        Session session = HibernateUtil.getSessionFactory().openSession();

        // get List of classes
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ClassTeacher> cqct = cb.createQuery(ClassTeacher.class);
        Root<ClassTeacher> ct = cqct.from(ClassTeacher.class);

        Predicate p = cb.equal(ct.get("teacher"), id);
        cqct.select(ct).where(p);

        Query<ClassTeacher> query = session.createQuery(cqct);
        List<ClassTeacher> classTeachers = query.list();

        Set<Integer> ids = new HashSet<>();
        for (ClassTeacher cte : classTeachers) {
            ids.add(cte.getGroup().getId());
        }

        CriteriaQuery<Learngroup> cqlg = cb.createQuery(Learngroup.class);
        Root<Learngroup> rootlg = cqlg.from(Learngroup.class);
        Query<Learngroup> querylg = session.createQuery(cqlg);
        List<Learngroup> learngroups = querylg.getResultList();

        for (Learngroup lg : learngroups) {
            if (ids.contains(lg.getId())) {
                LearngroupForLearnplaceDto lgDto = lg.toLearnplaceDto();
                lgDto.setTasks(getTasksForLearngroup(lg.getId(), session));
                lgDto.setStudents(getStudentsForLearngroup(lg.getId(), session));
                l.add(lgDto);
            }
        }

        dto.setClasses(l);
        session.close();
        return dto;
    }

    private List<StudentForTeacherDto> getStudentsForLearngroup(Integer id, Session session) {
        List<StudentForTeacherDto> dtos = new ArrayList<>();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ClassStudent> cq = cb.createQuery(ClassStudent.class);
        Root<ClassStudent> root = cq.from(ClassStudent.class);
        Predicate p = cb.equal(root.get("group"), id);
        cq.select(root).where(p);
        Query<ClassStudent> query = session.createQuery(cq);
        List<ClassStudent> cs = query.getResultList();

        for (ClassStudent c : cs) {
            dtos.add(c.getStudent().toForTeacherDto(""));
        }

        return dtos;
    }

    private List<TaskDto> getTasksForLearngroup(Integer id, Session session) {
        List<TaskDto> dtos = new ArrayList<>();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        Root<Task> root = cq.from(Task.class);
        Predicate p = cb.equal(root.get("learngroup"), id);
        cq.select(root).where(p);
        Query<Task> query = session.createQuery(cq);
        List<Task> tasks = query.getResultList();

        for (Task t : tasks) {
            dtos.add(t.toDto());
        }

        return dtos;
    }

    public StudentLearnplaceDto getStudentLearnplaceById(Integer id) {
        List<TaskStudentForLearnplaceDto> todo = new ArrayList<>();
        List<TaskStudentForLearnplaceDto> finished = new ArrayList<>();

        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<TaskStudent> cq = cb.createQuery(TaskStudent.class);
        Root<TaskStudent> root = cq.from(TaskStudent.class);
        Predicate p = cb.equal(root.get("student"), id);
        cq.select(root).where(p);
        Query<TaskStudent> query = session.createQuery(cq);
        List<TaskStudent> taskStudents = query.getResultList();

        for (TaskStudent ts : taskStudents) {
            if (ts.getState() >= 0 && ts.getState() < TaskState.values().length) {
                TaskState state = TaskState.values()[ts.getState()];
                switch (state) {
                    case TODO:
                    case SEEN:
                        todo.add(ts.toLearnplaceDto());
                        break;
                    case REVIEW:
                    case FINISHED:
                    case FAILED:
                        finished.add(ts.toLearnplaceDto());
                        break;
                    default:
                        break;
                }
            }
        }

        StudentLearnplaceDto dto = new StudentLearnplaceDto();
        dto.setTodo(todo);
        dto.setFinished(finished);

        session.close();
        return dto;
    }
}
