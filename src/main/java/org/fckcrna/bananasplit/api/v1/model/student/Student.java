package org.fckcrna.bananasplit.api.v1.model.student;

import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "student")
public class Student implements Serializable {

    private static final long serialVersionUID = 371946030818333462L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "nickname", nullable = false)
    private String nickName;

    @Column(name = "password", nullable = false)
    private String password;

    @ManyToOne
    @JoinColumn(name = "class")
    private Learngroup group;

    public Student() {

    }

    public Student(Long id, String nickName, String password, Learngroup group) {
        this.id = id;
        this.nickName = nickName;
        this.password = password;
        this.group = group;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getNickName() {
        return nickName;
    }
    
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Learngroup getGroup() {
        return group;
    }

    public void setGroup(Learngroup group) {
        this.group = group;
    }

    public StudentDto toDto() {
        StudentDto dto = new StudentDto();
        dto.setId(id);
        dto.setNickName(nickName);
        dto.setGroup(group.getId());
        return dto;
    }

    public StudentForTeacherDto toForTeacherDto(String clean) {
        StudentForTeacherDto dto = new StudentForTeacherDto();
        dto.setId(id);
        dto.setNickName(nickName);
        dto.setGroup(group.getId());
        dto.setPassword(clean);
        return dto;
    }
}