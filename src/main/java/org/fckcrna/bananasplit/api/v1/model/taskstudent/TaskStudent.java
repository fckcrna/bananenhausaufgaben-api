package org.fckcrna.bananasplit.api.v1.model.taskstudent;

import org.fckcrna.bananasplit.api.v1.model.learnplace.TaskStudentForLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.student.Student;
import org.fckcrna.bananasplit.api.v1.model.task.Task;
import org.fckcrna.bananasplit.util.ConvertUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;

@Entity
@Table(name = "task_student")
public class TaskStudent implements Serializable {

    private static final long serialVersionUID = -8325629577460508630L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task")
    private Task task;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student")
    private Student student;

    @Column(name = "note")
    private String note;

    @Column(name = "picture")
    private Blob picture;

    @Column(name = "state")
    private Integer state;

    @Column(name = "submission_date")
    private Long submissionDate;

    public TaskStudent() {

    }

    public TaskStudent(Task task, Student student, String note, Blob picture, Integer state, Long submissionDate) {
        this.task = task;
        this.student = student;
        this.note = note;
        this.picture = picture;
        this.state = state;
        this.submissionDate = submissionDate;
    }

    public static TaskStudent fromTaskStudent(TaskStudent ts) {
        TaskStudent taskStudent = new TaskStudent();
        taskStudent.setId(ts.getId());
        taskStudent.setSubmissionDate(ts.getSubmissionDate());
        taskStudent.setNote(ts.getNote());
        taskStudent.setState(ts.getState());
        taskStudent.setStudent(ts.getStudent());
        taskStudent.setTask(ts.getTask());
        taskStudent.setPicture(ts.getPicture());
        return taskStudent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public Long getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Long submissionDate) {
        this.submissionDate = submissionDate;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Blob getPicture() {
        return picture;
    }

    public void setPicture(Blob picture) {
        this.picture = picture;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public TaskStudentDto toDto() {
        TaskStudentDto dto = new TaskStudentDto();
        dto.setId(id);
        dto.setNote(note);
        dto.setState(state);
        dto.setStudent(student.getId());
        dto.setSubmissionDate(submissionDate);
        dto.setTask(task.getId());
        try {
            dto.setPicture(ConvertUtil.blobToBase64String(picture));
        } catch (Exception e) {
            dto.setPicture("");
            e.printStackTrace();
        }
        return dto;
    }

    public TaskStudentForLearnplaceDto toLearnplaceDto() {
        TaskStudentForLearnplaceDto dto = new TaskStudentForLearnplaceDto();
        dto.setId(id);
        dto.setNote(note);
        dto.setState(state);
        dto.setStudent(student.getId());
        dto.setSubmissionDate(submissionDate);
        dto.setTask(task.toDto());
        try {
            dto.setPicture(ConvertUtil.blobToBase64String(picture));
        } catch (Exception e) {
            dto.setPicture("");
            e.printStackTrace();
        }
        return dto;
    }

    public TaskStudentForDetailDto toDetailDto() {
        TaskStudentForDetailDto dto = new TaskStudentForDetailDto();
        dto.setId(id);
        dto.setNote(note);
        dto.setState(state);
        dto.setStudent(student.getId());
        dto.setStudentName(student.getNickName());
        dto.setSubmissionDate(submissionDate);
        dto.setTask(task.getId());
        try {
            dto.setPicture(ConvertUtil.blobToBase64String(picture));
        } catch (Exception e) {
            dto.setPicture("");
            e.printStackTrace();
        }
        return dto;
    }
}