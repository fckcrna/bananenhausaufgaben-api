package org.fckcrna.bananasplit.api.v1.model.learngroup;

public class LearngroupDto {

    private Integer id;
    private Integer teacher;
    private String name;

    public LearngroupDto() {
        
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTeacher() {
        return teacher;
    }

    public void setTeacher(Integer teacher) {
        this.teacher = teacher;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}