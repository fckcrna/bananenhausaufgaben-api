package org.fckcrna.bananasplit.api.v1.model.student;

public class StudentDto {

    private Long id;
    private String nickName;
    private Integer group;

    public StudentDto() {

    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}