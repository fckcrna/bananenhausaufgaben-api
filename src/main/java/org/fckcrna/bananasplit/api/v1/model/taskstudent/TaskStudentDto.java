package org.fckcrna.bananasplit.api.v1.model.taskstudent;

public class TaskStudentDto {

    private Long id;
    private Long task;
    private Long student;
    private String note;
    private String picture;
    private Integer state;
    private Long submissionDate;

    public TaskStudentDto() {

    }

    public Long getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Long submissionDate) {
        this.submissionDate = submissionDate;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getStudent() {
        return student;
    }

    public void setStudent(Long student) {
        this.student = student;
    }

    public Long getTask() {
        return task;
    }

    public void setTask(Long task) {
        this.task = task;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}