package org.fckcrna.bananasplit.api.v1.student;

import org.fckcrna.bananasplit.api.v1.model.student.StudentForTeacherDto;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author Felix Bender
 * @version 1.0
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/v1/student")
public class StudentController {

    @Autowired
    StudentService service;

    @PostMapping(value = "/new")
    public StudentForTeacherDto addStudentToClass(@RequestBody final String body) {
        JSONParser parser = new JSONParser();

        try {
            JSONObject json = (JSONObject) parser.parse(body);
            return service.createStudentInClass(json);
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed body", e);
        }
    }

    @PostMapping(value = "/newStudents")
    public List<StudentForTeacherDto> addStudentsToClass(@RequestBody final String body) {
        JSONParser parser = new JSONParser();

        try {
            JSONObject json = (JSONObject) parser.parse(body);
            return service.createStudentsInClass(json);
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed body", e);
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity deleteStudent(@RequestParam String id) {
        try {
            Long studentId = Long.valueOf(id);
            service.deleteStudent(studentId);
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "malformed ID", e);
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
