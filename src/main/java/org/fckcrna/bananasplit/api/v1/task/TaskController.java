package org.fckcrna.bananasplit.api.v1.task;

import org.fckcrna.bananasplit.api.v1.model.learnplace.TaskStudentForLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.task.TaskBuilder;
import org.fckcrna.bananasplit.api.v1.model.task.TaskDetailDto;
import org.fckcrna.bananasplit.api.v1.model.task.TaskDto;
import org.fckcrna.bananasplit.api.v1.model.taskstudent.TaskStudentDto;
import org.fckcrna.bananasplit.api.v1.model.taskstudent.TaskStudentForDetailDto;
import org.fckcrna.bananasplit.util.ConvertUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.sql.SQLException;

/**
 * @author Felix Bender
 * @version 1.0
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/v1/task")
public class TaskController {

    @Autowired
    TaskService service;

    @PostMapping(value = "/new")
    public TaskDto addTask(@RequestBody final String body) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject json = (JSONObject) parser.parse(body);

            TaskBuilder builder = new TaskBuilder()
                    .setDueDate((Long) json.get("duedate"))
                    .setPicture(ConvertUtil.base64StringToBlob((String) json.get("picture")))
                    .setSubject((String) json.get("subject"))
                    .setText((String) json.get("text"))
                    .setTitle((String) json.get("title"))
                    .setCreationDate(System.currentTimeMillis());

            TaskDto dto = service.createTask(json, builder);
            return dto;
        } catch (ParseException | SQLException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed body", e);
        }
    }

    @GetMapping(value = "/details")
    public TaskDetailDto getStudentsForTask(@RequestParam final String id) {
        try {
            Long i = Long.valueOf(id);
            return service.getStudentsForTask(i);
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed ID", e);
        }
    }

    @PostMapping(value = "/seen")
    public TaskStudentForLearnplaceDto taskSeenForStudent(@RequestParam final String id) {
        try {
            Long l = Long.valueOf(id);
            return service.setTaskSeen(l);
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed ID", e);
        }
    }

    @PostMapping(value = "/approve")
    public TaskStudentForDetailDto taskApprovedByTeacher(@RequestParam final String id) {
        try {
            Long l = Long.valueOf(id);
            return service.approveTaskByTeacher(l);
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed ID", e);
        }
    }

    @PutMapping(value = "/submit")
    public TaskStudentDto submitTask(@RequestBody final String body) {
        JSONParser parser = new JSONParser();

        try {
            JSONObject json = (JSONObject) parser.parse(body);
            TaskStudentDto dto = service.submitTask(json);
            return dto;
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed body", e);
        }
    }
}
