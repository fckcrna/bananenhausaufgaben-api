package org.fckcrna.bananasplit.api.v1.model.learngroup;

import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class LearngroupBuilder {

    private Integer id;
    private Teacher owner;
    private String name;

    public LearngroupBuilder() {

    }

    public LearngroupBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public LearngroupBuilder setOwner(Teacher owner) {
        this.owner = owner;
        return this;
    }

    public LearngroupBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public Learngroup build() {
        return new Learngroup(id, owner, name);
    }
}
