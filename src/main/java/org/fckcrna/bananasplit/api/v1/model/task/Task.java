package org.fckcrna.bananasplit.api.v1.model.task;

import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;
import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;
import org.fckcrna.bananasplit.util.ConvertUtil;

import javax.persistence.*;
import java.sql.Blob;

@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner", nullable = false)
    private Teacher owner;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "class", nullable = false)
    private Learngroup learngroup;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "picture")
    private Blob picture;

    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "due_date", nullable = false)
    private Long dueDate;

    @Column(name = "subject", nullable = false)
    private String subject;

    @Column(name = "creation_date", nullable = false)
    private Long creationDate;

    public Task() {

    }

    public Task(Long id, Teacher owner, Learngroup learngroup, String title, Blob picture, String text, Long dueDate,
                String subject, Long creationDate) {
        this.id = id;
        this.owner = owner;
        this.learngroup = learngroup;
        this.title = title;
        this.picture = picture;
        this.text = text;
        this.dueDate = dueDate;
        this.subject = subject;
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Blob getPicture() {
        return picture;
    }

    public void setPicture(Blob picture) {
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Learngroup getLearngroup() {
        return learngroup;
    }

    public void setLearngroup(Learngroup learngroup) {
        this.learngroup = learngroup;
    }

    public Teacher getOwner() {
        return owner;
    }

    public void setOwner(Teacher owner) {
        this.owner = owner;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaskDto toDto() {
        TaskDto dto = new TaskDto();
        dto.setDueDate(dueDate);
        dto.setId(id);
        dto.setLearngroup(learngroup.getId());
        dto.setLearngroupName(learngroup.getName());
        dto.setSubject(subject);
        dto.setOwner(owner.getId());
        dto.setText(text);
        dto.setTitle(title);
        dto.setCreationDate(creationDate);
        try {
            dto.setPicture(ConvertUtil.blobToBase64String(picture));
        } catch (Exception e) {
            e.printStackTrace();
            dto.setPicture("");
        }
        return dto;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }
}