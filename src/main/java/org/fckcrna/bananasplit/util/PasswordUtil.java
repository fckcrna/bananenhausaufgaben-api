package org.fckcrna.bananasplit.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.server.ResponseStatusException;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class PasswordUtil {

    /**
     * generates password of given length
     *
     * @return password
     */
    public static String generatePasswordString() {
        List<String> names = getElements("names");
        List<String> chars = getElements("char");
        List<String> colors = getElements("colors");

        StringBuilder sb = new StringBuilder();

        sb.append(colors.get(new Random().nextInt(colors.size())));
        sb.append(chars.get(new Random().nextInt(chars.size())));
        sb.append(names.get(new Random().nextInt(names.size())));

        return sb.toString();
    }

    private static List<String> getElements(String name) {
        JSONParser parser = new JSONParser();
        ClassLoader loader = new PasswordUtil().getClass().getClassLoader();

        try (FileReader reader = new FileReader(Objects.requireNonNull(loader.getResource("alphabet.json")).getFile())) {
            JSONObject json = (JSONObject) parser.parse(reader);
            return (List<String>) json.get(name);
        } catch (IOException | ParseException e) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Password generation failed");
        }
    }

    /**
     * Encrypts given password
     *
     * @param password the password to encrypt
     * @return the hashed password
     */
    public static String encryptPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    /**
     * Checks the clean password against a hashed one
     *
     * @param clean the clean pw
     * @param hash  the hashed pw
     * @return whether the passwords match
     */
    public static boolean checkPassword(String clean, String hash) {
        return BCrypt.checkpw(clean, hash);
    }
}
