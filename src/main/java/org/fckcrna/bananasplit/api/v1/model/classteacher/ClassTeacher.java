package org.fckcrna.bananasplit.api.v1.model.classteacher;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;
import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;

@Entity
@Table(name = "class_teacher")
public class ClassTeacher implements Serializable {

    private static final long serialVersionUID = 5163798955090057088L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "class", nullable = false)
    private Learngroup group;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacher", nullable = false)
    private Teacher teacher;

    public ClassTeacher() {

    }

    public ClassTeacher(Integer id, Learngroup group, Teacher teacher) {
        this.id = id;
        this.group = group;
        this.teacher = teacher;
    }

    public Integer getId() {
        return id;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Learngroup getGroup() {
        return group;
    }

    public void setGroup(Learngroup group) {
        this.group = group;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}