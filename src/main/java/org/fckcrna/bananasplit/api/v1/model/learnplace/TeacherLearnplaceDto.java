package org.fckcrna.bananasplit.api.v1.model.learnplace;

import java.util.List;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class TeacherLearnplaceDto {

    private List<LearngroupForLearnplaceDto> classes;

    public TeacherLearnplaceDto() {

    }

    public List<LearngroupForLearnplaceDto> getClasses() {
        return classes;
    }

    public void setClasses(List<LearngroupForLearnplaceDto> classes) {
        this.classes = classes;
    }
}
