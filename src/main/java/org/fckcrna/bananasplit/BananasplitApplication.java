package org.fckcrna.bananasplit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BananasplitApplication {

	public static void main(String[] args) {
		SpringApplication.run(BananasplitApplication.class, args);
	}
}
