package org.fckcrna.bananasplit.api.v1.model.learnplace;

import java.util.List;

/**
 * @author Felix Bender
 * @version 1.0
 */
public class StudentLearnplaceDto {

    private List<TaskStudentForLearnplaceDto> todo;
    private List<TaskStudentForLearnplaceDto> finished;

    public StudentLearnplaceDto() {

    }

    public List<TaskStudentForLearnplaceDto> getTodo() {
        return todo;
    }

    public void setTodo(List<TaskStudentForLearnplaceDto> todo) {
        this.todo = todo;
    }

    public List<TaskStudentForLearnplaceDto> getFinished() {
        return finished;
    }

    public void setFinished(List<TaskStudentForLearnplaceDto> finished) {
        this.finished = finished;
    }
}
