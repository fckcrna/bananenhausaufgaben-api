package org.fckcrna.bananasplit.api.v1.model.learngroup;

import org.fckcrna.bananasplit.api.v1.model.learnplace.LearngroupForLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "class")
public class Learngroup implements Serializable {

    private static final long serialVersionUID = -5060393619214561178L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner")
    private Teacher owner;

    @Column(name = "name")
    private String name;

    public Learngroup() {

    }

    public Learngroup(Integer id, Teacher owner, String name) {
        this.id = id;
        this.owner = owner;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Teacher getOwner() {
        return owner;
    }

    public void setOwner(Teacher owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LearngroupDto toDto() {
        LearngroupDto dto = new LearngroupDto();
        dto.setId(id);
        dto.setName(name);
        dto.setTeacher(owner.getId());
        return dto;
    }

    public LearngroupForLearnplaceDto toLearnplaceDto() {
        LearngroupForLearnplaceDto dto = new LearngroupForLearnplaceDto();
        dto.setId(id);
        dto.setName(name);
        dto.setTeacherId(owner.getId());
        dto.setTeacherName(owner.getFirstName() + " " + owner.getLastName());
        return dto;
    }
}