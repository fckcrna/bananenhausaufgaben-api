package org.fckcrna.bananasplit.api.v1.learngroup;

import org.fckcrna.bananasplit.api.v1.model.learnplace.LearngroupForLearnplaceDto;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Felix Bender
 * @version 1.0
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1/class")
public class LearngroupController {

    @Autowired
    private LearngroupService service;

    @PostMapping(value = "/new")
    public LearngroupForLearnplaceDto addLearngroup(@RequestBody final String body) {
        JSONParser parser = new JSONParser();

        try {
            JSONObject json = (JSONObject) parser.parse(body);
            return service.createLearngroup(json);
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed body", e);
        }
    }
}
