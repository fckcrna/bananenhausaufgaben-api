package org.fckcrna.bananasplit.api.v1.learngroup;

import org.fckcrna.bananasplit.api.v1.model.classteacher.ClassTeacher;
import org.fckcrna.bananasplit.api.v1.model.learngroup.Learngroup;
import org.fckcrna.bananasplit.api.v1.model.learngroup.LearngroupBuilder;
import org.fckcrna.bananasplit.api.v1.model.learnplace.LearngroupForLearnplaceDto;
import org.fckcrna.bananasplit.api.v1.model.teacher.Teacher;
import org.fckcrna.bananasplit.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;

/**
 * @author Felix Bender
 * @version 1.0
 */
@Service
public class LearngroupService {

    public LearngroupForLearnplaceDto createLearngroup(JSONObject json) {
        // get Teacher for Learngroup
        Session session = HibernateUtil.getSessionFactory().openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Teacher> cqt = cb.createQuery(Teacher.class);
        Root<Teacher> root = cqt.from(Teacher.class);

        Predicate teacherPredicate = cb.equal(root.get("id"), (Long) json.get("teacherId"));
        cqt.select(root).where(teacherPredicate);

        Query<Teacher> query = session.createQuery(cqt);
        Teacher teacher = query.uniqueResult();
        if (teacher == null) {
            session.close();
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Teacher not found");
        }

        LearngroupBuilder builder = new LearngroupBuilder()
                .setName((String) json.get("name"))
                .setOwner(teacher);

        Learngroup learngroup = builder.build();

        session.save(learngroup);

        // save ClassTeacher
        ClassTeacher ct = new ClassTeacher();
        ct.setTeacher(teacher);
        ct.setGroup(learngroup);

        session.save(ct);

        session.close();

        LearngroupForLearnplaceDto dto = learngroup.toLearnplaceDto();
        dto.setTasks(new ArrayList<>());

        return dto;
    }
}
